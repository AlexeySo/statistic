
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс, демонстрирующий подсчет статистики текста
 * из файла
 *
 * @author Sobolev
 */
public class Statistics {
    private static Scanner sc = new Scanner(System.in);
private static final String QUALITY_OF_SYMBOLS = "Количество символов: ";
    private static final String QUALITY_OF_SYMBOLS_WITHOUT_SPACES = "Количество символов без пробела: ";
    private static final String QUALITY_OF_WORDS = "Количество слов: ";

    public static void main(String[] args) {
        try {
            String filePath = inputPath();
            String outputPath = inputPath();
            List<String> outputArray = new ArrayList<>();
            List<String> list = readFile(filePath);
            String text =fillToText(list);
            countSymbolsWithoutSpace(text, outputArray);
            countSymbolsWithSpace(text, outputArray);
            countWords(text,outputArray);
            writeFile(outputPath,outputArray);
            outputToConsole(outputArray);

        } catch (NoSuchFileException e) {
            System.out.println("Файл не найден!");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Заполнениен строки из списка
     * @param list список
     * @return строку
     */

    private static String fillToText(List<String> list) {
        String text = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            text = text +"\n" +list.get(i);
        }
        return text;
    }

    /**
     * Выводит результат в консоль
     *
     * @param outputArray список данных вывода
     */
    private static void outputToConsole(List<String> outputArray) {
        for (int i = 0; i < outputArray.size(); i++) {
            System.out.println(outputArray.get(i));
        }
    }
    /**
     * Считает количество слов в строке
     *
     * @param text исходный текст
     * @param list список
     */

    private static void countWords(String text, List<String> list) {
        String tmpText = text;
        tmpText = tmpText.replaceAll("\\pP"," ");
        tmpText = tmpText.replaceAll("\\s+"," ");
        String[] words = tmpText.split("[\n ]");
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
            list.add(2, QUALITY_OF_WORDS + words.length);

    }
    /**
     * Ввод пути пользователем
     *
     * @return путь, введенный пользователем
     */
    private static String inputPath() {
        System.out.println("Введите путь: ");
        String filePath = sc.nextLine();
        boolean oPath = filePath.matches("^[a-z0-9]([a-z0-9-]*[a-z0-9])?(/[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$");
        boolean aPath = filePath.matches("([a-zA-Z]:)?(\\\\[a-zA-Z0-9._-]+)+\\\\?");
        if (!filePath.isEmpty()) {
            if (oPath || aPath){
                return filePath;
            }else {
                System.out.println("Формат пути неверен!");
            }
        } else {
            System.out.println("Введите путь!");
        }
        return "";
    }

    /**
     * Чтение из файла
     *
     * @param filePath путь к файл
     *
     * @return список строк
     *
     * @throws IOException
     */
    private static List<String> readFile(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath));
    }

    /**
     * Считает количество символов без пробела в строке
     *
     * @param text исходный текст
     * @param list список
     */
    private static void countSymbolsWithoutSpace(String text, List<String> list) {
        String tmpText = text.replaceAll("\\s+", "");
        list.add(0,QUALITY_OF_SYMBOLS_WITHOUT_SPACES + (tmpText.length()-1));
    }
    /**
     * Считает количество символов с пробелами в строке
     *
     * @param text исходный текст
     * @param list список
     */
    private static void countSymbolsWithSpace(String text, List<String> list) {
        list.add(1,QUALITY_OF_SYMBOLS+(text.length()-1));
    }
    /**
     * Запись в файл
     *
     * @param filePath путь к файлу
     * @param list список вывода
     */
    private static void writeFile(String filePath, List<String> list) throws IOException {
         Files.write(Paths.get(filePath),list);
    }

}
